#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/platform_device.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Matthew Knight");
MODULE_DESCRIPTION("A simple test kernel module");
MODULE_VERSION("1.0");

static struct of_device_id hello_match_table[] = {
	{
		.compatible = "mknight,hello",
	},
	{},
};
MODULE_DEVICE_TABLE(of, hello_match_table);

// platform driver stuff
static char const* name = "world";
int hello_probe(struct platform_device* device) {
	struct device_node* node = device->dev.of_node;
	void const* ptr = of_get_property(node, "name-parameter", NULL);
	if (ptr) {
		name = ptr;
	}

	printk(KERN_INFO "HELLO: probe() called\n");
	return 0;
}

int hello_remove(struct platform_device* device) {
	printk(KERN_INFO "HELLO: remove() called\n");
	return 0;
}

static struct platform_driver hello_platform_driver = {
	.probe = hello_probe,
	.remove = hello_remove,
	.driver = {
		.name = "hello",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(hello_match_table),
	},
};

static int __init hello_init(void) {
	int status = platform_driver_register(&hello_platform_driver);
	if (status < 0) {
		printk(KERN_INFO "HELLO: failed to register platform driver\n");
		return status;
	}
    printk(KERN_INFO "HELLO: Hello %s from the hello kernel module!\n", name);
    return 0;
}

static void __exit hello_exit(void) {
	struct platform_driver* ptr = &hello_platform_driver;
	platform_unregister_drivers(&ptr, 1);
    printk(KERN_INFO "HELLO: Goodbye %s from the hello kernel module!\n", name);
}

module_init(hello_init);
module_exit(hello_exit);
